from typing import *

import torch
from omegaconf import DictConfig

from reclib.modeling.modules.abstract_modelmodule import RankingModelModule


class UserItemInnerProdRankingModule(RankingModelModule):
    def __init__(self,
                 feature_config: List[DictConfig],
                 feature_group_config: List[DictConfig],
                 model_config: List[DictConfig],
                 outputs: Dict[str, List[str]],
                 optimizer: torch.optim.Optimizer,
                 scheduler: torch.optim.lr_scheduler):
        super().__init__(outputs, optimizer, scheduler)
        self.model_config = model_config
        self.feature_config = feature_config
        self.feature_group_config = feature_group_config
