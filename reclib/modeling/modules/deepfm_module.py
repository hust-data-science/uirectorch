from typing import *

import torch
from omegaconf import DictConfig

from reclib.modeling.modules.abstract_modelmodule import RankingModelModule
from reclib.modeling.components.utils import build_module_pool, build_model_module




class WideDeepFMRankingModule(RankingModelModule):
    def __init__(self,
                 data_attributes: Dict[str, DictConfig],
                 feature_config: Dict[str, DictConfig],
                 feature_group_config: DictConfig,
                 model_config: DictConfig,
                 outputs: Dict[str, List[str]],
                 optimizer: torch.optim.Optimizer,
                 scheduler: torch.optim.lr_scheduler):
        super().__init__(outputs, optimizer, scheduler)
        self.model_config = model_config
        self.feature_config = feature_config
        self.feature_group_config = feature_group_config
        build_model_module(data_attributes, feature_config, feature_group_config, model_config)
