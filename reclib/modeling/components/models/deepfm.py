import torch
import torch.nn as nn
from torch import Tensor

from reclib.modeling.components.layers.base import DNN


class WideDeepFMModel(nn.Module):
    def __init__(self,
                 cont_module,
                 cat_module,
                 sparse_module,
                 emb_module,  # like cat_module
                 hist_module,
                 fm_cat_module,
                 tab_head_hidden_dims=[128],
                 tab_head_activation: str = "relu",
                 tab_head_dropout: float = 0.1,
                 tab_head_batchnorm: bool = False,
                 tab_head_batchnorm_last: bool = False,
                 tab_head_linear_first: bool = False,
                 tab_head_output_dim: int = 64,
                 head_hidden_dims=[256, 128],
                 head_activation: str = "relu",
                 head_dropout: float = 0.1,
                 head_batchnorm: bool = False,
                 head_batchnorm_last: bool = False,
                 head_linear_first: bool = False
                 ):
        super().__init__()
        self.cont_module = cont_module
        self.cat_module = cat_module
        self.hist_module = hist_module
        self.emb_module = emb_module
        self.sparse_module = sparse_module
        self.fm_cat_module = fm_cat_module  # with projector if FM layer
        tab_head_hidden_dims = [self.cat_module.output_dim + self.cont_module.output_dim] + tab_head_hidden_dims
        self.tab_head = nn.Sequential(
            DNN(
                tab_head_hidden_dims,
                tab_head_activation,
                tab_head_dropout,
                tab_head_batchnorm,
                tab_head_batchnorm_last,
                tab_head_linear_first,
            ),
            nn.Linear(tab_head_hidden_dims[-1], tab_head_output_dim),
        )

        self.tab_output_dim = tab_head_output_dim
        deep_dim = (self.sparse_module.output_dim +
                    self.emb_module.output_dim +
                    self.hist_module.output_dim +
                    self.fm_cat_module.output_dim +
                    tab_head_output_dim)
        head_hidden_dims = [deep_dim] + head_hidden_dims
        self.head = nn.Sequential(
            DNN(
                head_hidden_dims,
                head_activation,
                head_dropout,
                head_batchnorm,
                head_batchnorm_last,
                head_linear_first,
            ),
            nn.Linear(head_hidden_dims[-1], self.output_dim),
        )

    def forward(self, cont_inputs: Tensor, cat_inputs: Tensor, hist_inputs: Tensor):
        """
        :param hist_inputs:
        :param cont_inputs: bz, total_features (num_features = f1 * n_per_f1 + f2 * n_per_f2 common n_per_fi=1 i = 1 -> num_conts)
        :param cat_inputs: bz, total_features (num_features = f1 * n_per_f1 + f2 * n_per_f2 common n_per_fi=1 i = 1 -> num_cats)
        :return:
        """
        fm_outputs = self.fm_cat_module(cat_inputs)
        cont_outputs = self.cont_module(cont_inputs)
        cat_outputs = self.cat_module(cat_inputs)
        sparse_cat_outputs = self.sparse_cat_module(cat_outputs)
        emb_outputs = self.emb_module(cat_outputs)
        hist_outputs = self.hist_module(hist_inputs)

        tab_head_outputs = self.tab_head_module(torch.cat([
            cont_outputs,
            cat_outputs,
        ], dim=1))

        return self.head(torch.cat([
            fm_outputs,
            tab_head_outputs,
            sparse_cat_outputs,
            emb_outputs,
            hist_outputs
        ], dim=1))
