# from reclib.modeling.components.layers.attention import *
# from reclib.modeling.components.layers.feature_old import *
# from reclib.modeling.components.layers.group_old import *
# from reclib.modeling.components.layers.projector import *
# import reclib.modeling.modules.deepfm_module.WideDeepFMRankingModule
#
# def build_module_pool(feature_config):
#     module_map = {}
#     for key, config in feature_config.items():
#         feature_type = config["type"]
#
#         if "projector" in config:
#             projector_config = config["projector"]
#             projector_type = projector_config["type"]
#             if projector_type == "LinearProjector":
#                 config["projector"] = LinearProjector(**projector_config)
#
#         # single module
#         if feature_type == "IDFeature":
#             module_map[key] = IDFeature(**config)
#
#         elif feature_type == "ContFeature":
#             module_map[key] = ContFeature(**config)
#
#         elif feature_type == "CatFeature":
#             module_map[key] = CatFeature(**config)
#
#         elif feature_type == "RawFeature":
#             module_map[key] = RawFeature(**config)
#
#     for key, config in feature_config.items():
#         feature_type = config["type"]
#         if "aggregator" not in config:
#             continue
#
#         if "projector" in config["aggregator"]:
#             projector_config = config["aggregator"]["projector"]
#             projector_type = projector_config["type"]
#             if projector_type == "LinearProjector":
#                 config["aggregator"]["projector"] = LinearProjector(**projector_config)
#
#         if feature_type == "SeqFeature":
#             # if use same module above but change config -> not support
#             encoder = module_map[config["key"]]
#             aggregator_type = config["aggregator"]["type"]
#             if aggregator_type == "ExtShortTermAttention":
#                 aggregator = ExtShortTermAttention(**config["aggregator"])
#             elif aggregator_type == "LightShortTermAttention":
#                 aggregator = LightShortTermAttention(**config["aggregator"])
#             elif aggregator_type == "PersonalizedAttention":
#                 aggregator = PersonalizedAttention(**config["aggregator"])
#             else:
#                 aggregator = None
#
#             module_map[key] = SeqFeature(encoder, aggregator)
#
#     return module_map
#
#
# def build_feature_group(feature_module_pool, feature_group_config):
#     module_map = {}
#     for group_name, group_config in feature_group_config.items():
#         group_type = group_config["type"]
#         features = group_config["features"]
#
#         if "projector" in group_config:
#             projector_config = group_config["projector"]
#             projector_type = projector_config["type"]
#             if projector_type == "LinearProjector":
#                 group_config["projector"] = LinearProjector(**projector_config)
#
#         if group_type == "DeepDNNFeatureGroup":
#             input_dim = 0
#             for feat in features:
#                 feat_module = feature_module_pool[feat]
#                 input_dim += feat_module.output_dim
#
#             group_config["hidden_dims"] = [input_dim] + group_config["hidden_dims"]
#             module_map[group_type] = DeepDNNGroup(**group_config)
#
#         elif group_type == "ConcatEmbFeatureGroup":
#             group_config["output_dim"] = 0
#             for feat in features:
#                 feat_module = feature_module_pool[feat]
#                 group_config["output_dim"] += feat_module.output_dim
#
#             module_map[group_type] = ConcatEmbFeatureGroup(**group_config)
#
#         elif group_type == "DeepContFeatureGroup":
#             group_config["output_dim"] = len(features)
#             module_map[group_type] = DeepContFeatureGroup(**group_config)
#
#         elif group_type == "SparseCatFeatureGroup":
#             group_config["n_uniques"] = []
#             for _feat in group_config["features"]:
#                 group_config["n_uniques"].append(feature_module_pool[_feat].n_uniques)
#             module_map[group_type] = SparseCatFeatureGroup(**group_config)
#
#         else:
#             raise ValueError(f"Not supported feature group type {group_type}")
#
#     return module_map
