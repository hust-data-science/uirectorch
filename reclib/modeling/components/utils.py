from reclib.modeling.components.layers.attention import ExtShortTermAttention, PersonalizedAttention, \
    LightShortTermAttention
from reclib.modeling.components.layers.feature import IDFeatureLayer, CatFeatureLayer, RawFeatureLayer, SeqFeatureLayer, \
    ContFeatureLayer, EmbFeatureLayer
from reclib.modeling.components.layers.group import DeepContFeatureGroup, SparseAvgCatFeatureGroup
from reclib.modeling.components.layers.projector import LinearProjector


def get_input_feature_info(data_attributes, feature_config, feature_group_config):
    data_feats = set()
    feature_info = {feat: config for feat, config in feature_config.items()}
    for group_name, group_config in feature_group_config.items():
        features = group_config["features"]
        if type(features) == list:
            for feat in group_config["features"]:
                data_feats.add(feature_info[feat]["input"])

    cont_feats = []
    cat_feats = []
    hist_feats = []
    for input_feat in data_feats:
        if data_attributes[input_feat]["type"] == "cont":
            cont_feats.append(input_feat)
            continue

        if data_attributes[input_feat]["type"] == "cat":
            cat_feats.append(input_feat)
            continue

        if data_attributes[input_feat]["type"] == "hist":
            hist_feats.append(input_feat)
            continue

    count = 0
    cont_indices = []
    for feat in cont_feats:
        cont_indices.append(count)
        count += data_attributes[feat].get("res_size", 1)

    count = 0
    cat_indices = []
    for feat in cat_feats:
        cat_indices.append(count)
        count += data_attributes[feat].get("res_size", 1)

    count = 0
    hist_indices = []
    for feat in hist_feats:
        hist_indices.append(count)
        count += data_attributes[feat].get("res_size", 1)

    acc_cont_indices = []
    for i in range(len(cont_indices)):
        if i < len(cont_indices) - 1:
            acc_cont_indices.append([j for j in range(cont_indices[i], cont_indices[i + 1], 1)])
        else:
            acc_cont_indices.append([j for j in range(cont_indices[i], sum(cont_indices), 1)])

    print(cont_feats, cat_feats, hist_feats)
    acc_cat_indices = []
    for i in range(len(cat_indices)):
        if i < len(cat_indices) - 1:
            acc_cat_indices.append([j for j in range(cat_indices[i], cat_indices[i + 1], 1)])
        else:
            acc_cat_indices.append([j for j in range(cat_indices[i], sum(cat_indices), 1)])

    acc_hist_indices = []
    for i in range(len(hist_indices)):
        if i < len(hist_indices) - 1:
            acc_hist_indices.append([j for j in range(hist_indices[i], hist_indices[i + 1], 1)])
        else:
            acc_hist_indices.append([j for j in range(hist_indices[i], sum(hist_indices), 1)])

    cont_feats = {feat: indices for feat, indices in zip(cont_indices, acc_cont_indices)}
    cat_feats = {feat: indices for feat, indices in zip(cat_feats, acc_cat_indices)}
    hist_feats = {feat: indices for feat, indices in zip(hist_feats, acc_hist_indices)}
    return cont_feats, cat_feats, hist_feats


def build_model_module(data_attributes, feature_config, feature_group_config, model_config):
    cont_feats, cat_feats, hist_feats = get_input_feature_info(data_attributes,
                                                               feature_config,
                                                               feature_group_config)
    print(cont_feats, cat_feats, hist_feats)
    feature_info = {feat: config for feat, config in feature_config.items()}
    feature_module_pool = build_module_pool(feature_config)
    group2features = {}
    for group_name, group_config in feature_group_config.items():
        group2features[group_name] = group_config["features"]

    feature_group_module_pool = {}
    for group_name, group_config in model_config.items():
        group_type = group_config["type"]

        if "projector" in group_config:
            projector_config = group_config["projector"]
            projector_type = projector_config["type"]
            if projector_type == "LinearProjector":
                group_config["projector"] = LinearProjector(**projector_config)

        if group_type == "DeepContFeatureGroup":
            output_dim = 0
            for feat in group2features[group_name]:
                feat_module = feature_module_pool[feat]
                output_dim += feat_module.output_dim

            if "output_dim" in group_config:
                assert group_config["output_dim"] == output_dim

            group_config["output_dim"] = output_dim
            feature_group_module_pool[group_type] = DeepContFeatureGroup(**group_config)

        elif group_type == "SparseAvgCatFeatureGroup":
            feature_unique_values = []
            num_feats = len(group2features[group_name])
            for feat in group2features[group_name]:
                feature_unique_values.append(feature_info[feat]["n_uniques"])

            group_config["num_feats"] = num_feats
            group_config["n_uniques"] = feature_unique_values
            group_config["feat_indices"] = [cat_feats[feature_info[feat]["input"]] for feat in group2features[group_name]]

            feature_group_module_pool[group_type] = SparseAvgCatFeatureGroup(**group_config)
        # elif group_type == "DeepContFeatureGroup":
        #     group_config["output_dim"] = len(features)
        #     feature_group_module_pool[group_type] = DeepContFeatureGroup(**group_config)
        #
        # elif group_type == "SparseCatFeatureGroup":
        #     group_config["n_uniques"] = []
        #     for _feat in group_config["features"]:
        #         group_config["n_uniques"].append(feature_module_pool[_feat].n_uniques)
        #     feature_group_module_pool[group_type] = SparseCatFeatureGroup(**group_config)
        #
        # else:
        #     raise ValueError(f"Not supported feature group type {group_type}")

    return feature_group_module_pool


def build_module_pool(feature_config):
    module_map = {}
    for key, config in feature_config.items():
        feature_type = config["type"]

        if "projector" in config:
            projector_config = config["projector"]
            projector_type = projector_config["type"]
            if projector_type == "LinearProjector":
                config["projector"] = LinearProjector(**projector_config)

        # single module
        if feature_type == "IDFeatureLayer":
            module_map[key] = IDFeatureLayer(**config)

        elif feature_type == "ContFeatureLayer":
            module_map[key] = ContFeatureLayer(**config)

        elif feature_type == "CatFeatureLayer":
            module_map[key] = CatFeatureLayer(**config)

        elif feature_type == "RawFeatureLayer":
            module_map[key] = RawFeatureLayer(**config)

        elif feature_type == "EmbFeatureLayer":
            module_map[key] = EmbFeatureLayer(**config)

    for key, config in feature_config.items():
        feature_type = config["type"]
        if "aggregator" not in config:
            continue

        if "projector" in config["aggregator"]:
            projector_config = config["aggregator"]["projector"]
            projector_type = projector_config["type"]
            if projector_type == "LinearProjector":
                config["aggregator"]["projector"] = LinearProjector(**projector_config)

        if feature_type == "SeqFeatureLayer":
            # if use same module above but change config -> not support
            encoder = module_map[config["key"]]
            aggregator_type = config["aggregator"]["type"]
            if aggregator_type == "ExtShortTermAttention":
                aggregator = ExtShortTermAttention(**config["aggregator"])
            elif aggregator_type == "LightShortTermAttention":
                aggregator = LightShortTermAttention(**config["aggregator"])
            elif aggregator_type == "PersonalizedAttention":
                aggregator = PersonalizedAttention(**config["aggregator"])
            else:
                aggregator = None

            module_map[key] = SeqFeatureLayer(encoder, aggregator)

    return module_map
