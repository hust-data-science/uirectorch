import torch
import torch.nn as nn


class FM(nn.Module):
    """Factorization Machine models pairwise (order-2) feature interactions
     without linear term and bias.
      Input shape
        - 3D tensor with shape: ``(batch_size,field_size,embedding_size)``.
      Output shape
        - 2D tensor with shape: ``(batch_size, 1)``.
      References
        - [Factorization Machines](https://www.csie.ntu.edu.tw/~b97053/paper/Rendle2010FM.pdf)
    """

    def __init__(self, num_feats, emb_size):
        super(FM, self).__init__()
        self.num_feats = num_feats
        self.emb_size = emb_size

    def forward(self, inputs):
        """
        :param inputs: batch_size, num_module, embedding_size
        :return: batch_size, embedding_size
        """
        fm_input = inputs.view(inputs.size()[0], self.num_feats, self.emb_size)

        square_of_sum = torch.pow(torch.sum(fm_input, dim=1, keepdim=True), 2)
        sum_of_square = torch.sum(fm_input * fm_input, dim=1, keepdim=True)
        cross_term = square_of_sum - sum_of_square
        cross_term = 0.5 * torch.sum(cross_term, dim=2, keepdim=False)

        return cross_term.view(inputs.size()[0], -1)
