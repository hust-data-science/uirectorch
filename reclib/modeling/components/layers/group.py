import math
from typing import *

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor

from reclib.modeling.components.layers.base import get_activation_fn
from reclib.modeling.components.layers.feature import ContFeatEmbeddingLayer, CatFeatureLayer, EmbFeatureLayer, \
    SeqFeatureLayer


################# Cont Module #####################
class DeepContFeatureGroup(nn.Module):
    def __init__(self,
                 output_dim,  # num cont features
                 norm_layer: str = "batch_norm",
                 use_comb_cont: bool = False,
                 comb_cont_dim: int = 0,
                 use_bias: bool = True,
                 activation: str = None,
                 dropout: float = 0.1,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        if use_comb_cont:
            assert comb_cont_dim > 0
        self.cont_layer = nn.Identity()
        if norm_layer == "batch_norm":
            self.cont_layer = nn.BatchNorm1d(output_dim)
        elif norm_layer == "layer_norm":
            self.cont_layer = nn.LayerNorm(output_dim)

        self.output_dim = output_dim
        self.comb_cont_layer = nn.Identity()
        if use_comb_cont:
            self.comb_cont_layer = ContFeatEmbeddingLayer(
                output_dim,
                comb_cont_dim,
                use_bias,
                dropout
            )

            self.output_dim = output_dim * comb_cont_dim

        self.act = nn.Identity()
        if activation is not None:
            self.act = get_activation_fn(activation)

        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        """
        fix directly without output temp from each cont layer
        :param inputs: batch_size, num_cont_feat
        :return:
        """
        x = self.cont_layer(inputs)
        x = self.comb_cont_layer(x)
        x = self.act(x)
        return self.projector(x)


class AutoDisContFeatureGroup(nn.Module):
    def __init__(self,
                 num_feats: int,
                 emb_dim: int,
                 num_bins: int,
                 temperature: float,
                 keep_prob: float = 0.8,
                 min_val: int = None,
                 max_val: int = None,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.num_feats = num_feats
        self.num_bins = num_bins
        self.keep_prob = keep_prob
        self.temperature = temperature
        self.emb_dim = emb_dim
        self.min_val = min_val
        self.max_val = max_val
        self.meta_emb = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(num_feats, num_bins, emb_dim)), a=math.sqrt(5)
        )

        self.project_w = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(num_feats, num_bins)), a=math.sqrt(5)
        )

        self.project_mat = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(num_feats, num_bins, num_bins)), a=math.sqrt(5)
        )

        self.act = nn.LeakyReLU()
        self.output_dim = self.num_feats * self.emb_dim
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        """

        :param inputs:
        :param x: batch_size, num_cont_feats
        :return:
        """
        bz = inputs.size()[0]
        hidden = F.leaky_relu(self.project_w * inputs.view(bz, self.num_feats, 1))
        hidden = hidden.view(bz, self.num_feats, self.num_bins)  # [batch_size, num_cont_feats, num_bin]

        x = torch.matmul(self.project_mat, hidden.view(bz, self.num_feats, self.num_bins, 1))
        x = x.view(bz, self.num_feats, self.num_bins)

        x = x + self.keep_prob * hidden
        x = F.softmax(x / self.temperature, dim=2)  # [B, N, num_bin]
        x = torch.matmul(x.view(bz, self.num_feats, 1, self.num_bins), self.meta_emb)
        x = x.view(bz, self.num_feats * self.emb_dim)
        return self.projector(x)


################## Sparse Cat Module ####################
class SparseAvgCatFeatureGroup(nn.Module):
    def __init__(self,
                 n_uniques: List[int],
                 feat_indices: List[List[int]],
                 num_feats: int,
                 output_dim: int,
                 use_bias: bool = False,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        assert len(n_uniques) == num_feats
        assert len(feat_indices) == num_feats
        self.num_feats = num_feats
        self.feat_indices = [torch.tensor(indices, requires_grad=False).long() for indices in feat_indices]
        self.feat_start_tensor = torch.zeros(len(n_uniques), requires_grad=False).long()
        unique_size = 0
        for i, feat_unique in enumerate(n_uniques):
            self.feat_start_tensor[i] = unique_size
            unique_size += feat_unique

        self.wide_layer = nn.Embedding(unique_size, output_dim, padding_idx=0)
        self.bias = nn.Parameter(torch.zeros(1), requires_grad=False)
        if use_bias:
            self.bias = nn.Parameter(torch.zeros(output_dim))
        self._reset_parameters()
        self.output_dim = output_dim
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def _reset_parameters(self) -> None:
        nn.init.kaiming_uniform_(self.wide_layer.weight, a=math.sqrt(5))
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.wide_layer.weight)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.bias, -bound, bound)

    def forward_index(self, bz: int, index: int, inputs: Tensor):
        indices = self.feat_indices[index]
        x_i = inputs[:, indices].view(-1, 1) + self.feat_start_tensor[index]
        x_i = self.wide_layer(x_i.long())
        x_i = x_i.view(bz, indices.size()[0], -1)
        # avg by each feat_indices
        x_i = x_i.sum(dim=1) / indices.size()[0]
        return x_i.view(bz, 1, self.output_dim)

    def forward(self, inputs: Tensor):
        """
        :param inputs: [batch_size, num_cat_features ]
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            feat_0: [0]
            feat_1: [2, 3]
            feat_2: [4, 5, 6, 7]
            feat_3: [8, 9, 10]
            x = []
            for i in range(5)
                x_i = inputs[:, feat_indices].view(-1, 1) + self.feat_start_tensor[i] # -1, output_dim
                x_i = x_i.view(bz, -1).sum(dim=1)
                x.append(x_i)

            x = torch.cat(x, dim=1).sum(dim=1)

        :return:
        """
        bz = inputs.size()[0]
        x = []
        for i in range(self.num_feats):
            x.append(self.forward_index(bz, i, inputs))

        x = torch.cat(x, dim=1).sum(dim=1)
        x = x + self.bias
        return self.projector(x)


class SparseAvgCatFeatureGroup3(SparseAvgCatFeatureGroup):
    def __init__(self,
                 n_uniques: List[int],
                 feat_indices: List[List[int]],
                 output_dim: int,
                 use_bias: bool = False,
                 projector: nn.Module = None,
                 **_):
        super().__init__(n_uniques,
                         feat_indices,
                         3,
                         output_dim,
                         use_bias,
                         projector,
                         **_)

    def forward(self, inputs: Tensor):
        bz = inputs.size()[0]
        x = [
            self.forward_index(bz, 0, inputs),
            self.forward_index(bz, 1, inputs),
            self.forward_index(bz, 2, inputs)
        ]
        x = torch.cat(x, dim=1).sum(dim=1)
        x = x.sum(dim=1) + self.bias
        return self.projector(x)


################## Dense Cat Module ####################
class DenseEmbFeatureGroup(nn.Module):
    def __init__(self,
                 cat_modules,
                 feat_indices: List[List[int]],
                 num_feats: int,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        assert len(cat_modules) == num_feats
        assert len(feat_indices) == num_feats
        self.num_feats = num_feats
        self.feat_indices = [torch.tensor(indices, requires_grad=False).long() for indices in feat_indices]
        self.cat_modules = cat_modules
        self.projector = nn.Identity()
        self.output_dim = sum([cat_modules[i].output_dim for i in range(len(cat_modules))])
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs):
        bz = inputs.size()[0]
        return torch.cat([
            self.cat_modules[i](inputs[:, self.feat_indices[i]]).view(bz, -1)
            for i in range(self.num_modules)
        ], dim=1)


class DenseEmbFeatureGroup3(DenseEmbFeatureGroup):
    def __init__(self,
                 cat_modules,
                 feat_indices: List[List[int]],
                 projector: nn.Module = None,
                 **_):
        super().__init__(cat_modules,
                         feat_indices,
                         3,
                         projector,
                         **_)

    def forward(self, inputs):
        return self.projector(torch.cat([
            self.cat_modules[0](inputs),
            self.cat_modules[1](inputs),
            self.cat_modules[2](inputs),
        ], dim=1))


class DenseEmbFeatureGroup4(DenseEmbFeatureGroup):
    def __init__(self,
                 cat_modules,
                 feat_indices: List[List[int]],
                 projector: nn.Module = None,
                 **_):
        super().__init__(cat_modules,
                         feat_indices,
                         4,
                         projector,
                         **_)

    def forward(self, inputs):
        return self.projector(torch.cat([
            self.cat_modules[0](inputs),
            self.cat_modules[1](inputs),
            self.cat_modules[2](inputs),
            self.cat_modules[3](inputs),
        ], dim=1))


if __name__ == "__main__":
    s = SparseAvgCatFeatureGroup([4, 5, 6], [[7], [1, 2], [9]], 3, 8, use_bias=True)
    x = torch.zeros((12, 10))
    print(s(x).size())
