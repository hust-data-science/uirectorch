import math

import torch
import torch.nn as nn
from torch import Tensor


class ContFeatEmbeddingLayer(nn.Module):
    def __init__(
            self,
            n_cont_cols: int,
            output_dim: int,
            use_bias: bool,
            dropout: float):
        super(ContFeatEmbeddingLayer, self).__init__()

        self.n_cont_cols = n_cont_cols
        self.use_bias = use_bias

        self.weight = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(n_cont_cols, output_dim)), a=math.sqrt(5)
        )

        self.bias = (
            nn.init.kaiming_uniform_(
                nn.Parameter(torch.Tensor(n_cont_cols, output_dim)), a=math.sqrt(5)
            )
            if use_bias
            else nn.Parameter(torch.zeros(1), requires_grad=False)
        )

        self.dropout = nn.Identity()
        if dropout > 0:
            self.dropout = nn.Dropout(p=dropout)

    def forward(self, X: Tensor) -> Tensor:
        x = self.weight.unsqueeze(0) * X.unsqueeze(2)
        x = x + self.bias.unsqueeze(0)
        return self.dropout(x)


class CatFeatureLayer(nn.Module):
    def __init__(self,
                 n_uniques: int,
                 emb_dim: int,
                 emb_data: torch.Tensor = None,
                 emb_freeze: bool = True,
                 use_bias: bool = False,
                 dropout: float = 0,
                 is_sparse: bool = False,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.n_uniques = n_uniques
        self.layer = nn.Embedding(n_uniques, emb_dim, padding_idx=0, sparse=is_sparse)
        if emb_data is not None:
            assert emb_data.size()[1] == emb_dim
            self.layer = nn.Embedding.from_pretrained(emb_data, freeze=emb_freeze, sparse=is_sparse)

        self.bias = nn.Parameter(torch.zeros(1), requires_grad=False)
        if use_bias:
            bound = 1 / math.sqrt(emb_dim)
            self.bias = nn.Parameter(
                nn.init.uniform_(torch.Tensor(emb_dim), -bound, bound))

        self.dropout = nn.Identity()
        if dropout > 0:
            self.dropout = nn.Dropout(p=dropout)
        self.output_dim = emb_dim
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs):
        x = self.layer(inputs.long())
        x = x + self.bias
        x = self.dropout(x)
        return self.projector(x)


class RawFeatureLayer(nn.Module):
    def __init__(self,
                 output_dim: int = 1,
                 min_val: int = None,
                 max_val: int = None,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.min_val = min_val
        self.max_val = max_val
        self.output_dim = output_dim
        self.layer = nn.Identity()
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        return self.layer(inputs)


class ContFeatureLayer(nn.Module):
    def __init__(self,
                 emb_dim: int,
                 use_bn: bool = True,
                 dropout: float = 0,
                 min_val: int = None,
                 max_val: int = None,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.linear = nn.Linear(1, emb_dim) if emb_dim > 1 else nn.Identity()
        self.batch_norm = nn.BatchNorm1d(emb_dim) if use_bn else nn.Identity()
        self.dropout = nn.Identity()
        if dropout > 0:
            self.dropout = nn.Dropout(p=dropout)
        self.output_dim = emb_dim
        self.min_val = min_val
        self.max_val = max_val
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        x = self.linear(inputs.view(inputs.size()[0], -1))
        x = self.batchnorm(x)
        x = self.dropout(x)
        return self.projector(x)


class EmbFeatureLayer(CatFeatureLayer):
    def __init__(self,
                 n_uniques,
                 emb_dim,
                 emb_data: torch.Tensor = None,
                 emb_freeze: bool = True,
                 use_bias: bool = False,
                 dropout: float = 0,
                 is_sparse: bool = True,
                 projector: nn.Module = None,
                 **_):
        super().__init__(n_uniques,
                         emb_dim,
                         emb_data,
                         emb_freeze,
                         use_bias,
                         dropout,
                         is_sparse,
                         projector,
                         **_)


class IDFeatureLayer(CatFeatureLayer):
    def __init__(self,
                 n_uniques,
                 emb_dim,
                 emb_data: torch.Tensor = None,
                 emb_freeze: bool = False,
                 use_bias: bool = False,
                 dropout: float = 0,
                 is_sparse: bool = True,
                 projector: nn.Module = None,
                 **_):
        super().__init__(n_uniques,
                         emb_dim,
                         emb_data,
                         emb_freeze,
                         use_bias,
                         dropout,
                         is_sparse,
                         projector,
                         **_)


class SeqFeatureLayer(nn.Module):
    def __init__(self,
                 encoder,
                 aggregator,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.encoder = encoder
        self.aggregator = aggregator
        self.output_dim = aggregator.output_dim
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs, mask=None):
        """
        :param inputs:
        :param x: batch_size, item_size for_example: [[12, 212, 323, 0], [121, 3,4, 3], [0, 0, 0, 0]] -> max_length = 4
        :param mask:
        :return:
        """
        x = self.encoder(inputs)
        x = self.aggregator(x, mask)
        return self.projector(x)
