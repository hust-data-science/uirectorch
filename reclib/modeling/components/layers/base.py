from typing import *

import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor


class GEGLU(nn.Module):
    def forward(self, x):
        x, gates = x.chunk(2, dim=-1)
        return x * F.gelu(gates)


class REGLU(nn.Module):
    def forward(self, x):
        x, gates = x.chunk(2, dim=-1)
        return x * F.gelu(gates)


def get_activation_fn(activation):
    if activation == "relu":
        return nn.ReLU(inplace=True)
    elif activation == "leaky_relu":
        return nn.LeakyReLU(inplace=True)
    elif activation == "tanh":
        return nn.Tanh()
    elif activation == "gelu":
        return nn.GELU()
    elif activation == "geglu":
        return GEGLU()
    elif activation == "reglu":
        return REGLU()
    elif activation == "softplus":
        return nn.Softplus()
    else:
        raise ValueError(
            "Only the following activation functions are currently "
            "supported: {}. Note that 'geglu' and 'reglu' "
            "should only be used as transformer's activations")


def dense_layer(
        inp: int,
        out: int,
        activation: str,
        p: float,
        bn: bool,
        linear_first: bool,
):
    # This is basically the LinBnDrop class at the fastai library
    act_fn = get_activation_fn(activation)
    layers = [nn.BatchNorm1d(out if linear_first else inp)] if bn else []
    if p != 0:
        layers.append(nn.Dropout(p))  # type: ignore[arg-type]
    lin = [nn.Linear(inp, out, bias=not bn), act_fn]
    layers = lin + layers if linear_first else layers + lin
    return nn.Sequential(*layers)


class DNN(nn.Module):
    def __init__(
            self,
            hidden_dims: List[int],
            activation: str,
            dropout: Optional[Union[float, List[float]]],
            use_bn: bool,
            use_bn_last: bool,
            use_ln_first: bool
    ):
        super(DNN, self).__init__()

        if not dropout:
            dropout = [0.0] * len(hidden_dims)
        elif isinstance(dropout, float):
            dropout = [dropout] * len(hidden_dims)

        self.mlp = nn.Sequential()
        for i in range(1, len(hidden_dims)):
            self.mlp.add_module(
                "dense_layer_{}".format(i - 1),
                dense_layer(
                    hidden_dims[i - 1],
                    hidden_dims[i],
                    activation,
                    dropout[i - 1],
                    use_bn and (i != len(hidden_dims) - 1 or use_bn_last),
                    use_ln_first,
                ),
            )

    def forward(self, X: Tensor) -> Tensor:
        return self.mlp(X)
