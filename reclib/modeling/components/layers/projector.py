from typing import *

from reclib.modeling.components.layers.base import DNN


class LinearProjector(DNN):
    def __init__(self,
                 hidden_dims: List[int],
                 activation: str,
                 dropout: Optional[Union[float, List[float]]],
                 use_bn: bool,
                 use_bn_last: bool,
                 use_ln_first: bool):
        super().__init__(hidden_dims,
                         activation,
                         dropout,
                         use_bn,
                         use_bn_last,
                         use_ln_first)
