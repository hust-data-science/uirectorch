import math
from typing import *

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import Tensor

from reclib.modeling.components import AdvCatFeature
from reclib.modeling.components.layers.base import get_activation_fn, DNN


class ContFeatEmbedding(nn.Module):
    def __init__(
            self,
            n_cont_cols: int,
            output_dim: int,
            use_bias: bool,
            dropout: float):
        super(ContFeatEmbedding, self).__init__()

        self.n_cont_cols = n_cont_cols
        self.use_bias = use_bias

        self.weight = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(n_cont_cols, output_dim)), a=math.sqrt(5)
        )

        self.bias = (
            nn.init.kaiming_uniform_(
                nn.Parameter(torch.Tensor(n_cont_cols, output_dim)), a=math.sqrt(5)
            )
            if use_bias
            else nn.Parameter(torch.zeros(1), requires_grad=False)
        )

        self.dropout = nn.Identity()
        if dropout > 0:
            self.dropout = nn.Dropout(p=dropout)

    def forward(self, X: Tensor) -> Tensor:
        x = self.weight.unsqueeze(0) * X.unsqueeze(2)
        x = x + self.bias.unsqueeze(0)
        return self.dropout(x)


class SparseCatFeatureGroup(nn.Module):
    def __init__(self,
                 n_uniques: List[int],
                 output_dim: int,
                 use_bias: bool = False,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.feat_start_tensor = torch.zeros(len(n_uniques)).long()
        unique_size = 0
        for i, feat_unique in enumerate(n_uniques):
            self.feat_start_tensor[i] = unique_size
            unique_size += feat_unique

        self.wide_layer = nn.Embedding(unique_size, output_dim, padding_idx=0)
        self.bias = nn.Parameter(torch.zeros(1), requires_grad=False)
        if use_bias:
            self.bias = nn.Parameter(torch.zeros(output_dim))
        self._reset_parameters()
        self.output_dim = output_dim
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def _reset_parameters(self) -> None:
        nn.init.kaiming_uniform_(self.wide_layer.weight, a=math.sqrt(5))
        fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.wide_layer.weight)
        bound = 1 / math.sqrt(fan_in)
        nn.init.uniform_(self.bias, -bound, bound)

    def forward(self, inputs: Tensor):
        """
        :param inputs: [batch_size, num_features]
        :return:
        """
        x = inputs.long() + self.feat_start_tensor
        x = self.wide_layer(x)  # -> batch_size, num_features, output_dim
        x = x.sum(dim=1) + self.bias
        return self.projector(x)


class AutoDisContFeatureGroup(nn.Module):
    def __init__(self,
                 num_feats: int,
                 emb_dim: int,
                 num_bins: int,
                 temperature: float,
                 keep_prob: float = 0.8,
                 min_val: int = None,
                 max_val: int = None,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.num_feats = num_feats
        self.num_bins = num_bins
        self.keep_prob = keep_prob
        self.temperature = temperature
        self.emb_dim = emb_dim
        self.min_val = min_val
        self.max_val = max_val
        self.meta_emb = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(num_feats, num_bins, emb_dim)), a=math.sqrt(5)
        )

        self.project_w = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(num_feats, num_bins)), a=math.sqrt(5)
        )

        self.project_mat = nn.init.kaiming_uniform_(
            nn.Parameter(torch.Tensor(num_feats, num_bins, num_bins)), a=math.sqrt(5)
        )

        self.act = nn.LeakyReLU()
        self.output_dim = self.num_feats * self.emb_dim
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        """

        :param inputs:
        :param x: batch_size, num_cont_feats
        :return:
        """
        bz = inputs.size()[0]
        hidden = F.leaky_relu(self.project_w * inputs.view(bz, self.num_feats, 1))
        hidden = hidden.view(bz, self.num_feats, self.num_bins)  # [batch_size, num_cont_feats, num_bin]

        x = torch.matmul(self.project_mat, hidden.view(bz, self.num_feats, self.num_bins, 1))
        x = x.view(bz, self.num_feats, self.num_bins)

        x = x + self.keep_prob * hidden
        x = F.softmax(x / self.temperature, dim=2)  # [B, N, num_bin]
        x = torch.matmul(x.view(bz, self.num_feats, 1, self.num_bins), self.meta_emb)
        x = x.view(bz, self.num_feats * self.emb_dim)
        return self.projector(x)


class DeepContFeatureGroup(nn.Module):
    def __init__(self,
                 output_dim,  # num cont features
                 norm_layer: str = "batch_norm",
                 use_comb_cont: bool = False,
                 comb_cont_dim: int = 0,
                 use_bias: bool = True,
                 activation: str = None,
                 dropout: float = 0.1,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        if use_comb_cont:
            assert comb_cont_dim > 0
        self.cont_layer = nn.Identity()
        if norm_layer == "batch_norm":
            self.cont_layer = nn.BatchNorm1d(output_dim)
        elif norm_layer == "layer_norm":
            self.cont_layer = nn.LayerNorm(output_dim)

        self.output_dim = output_dim
        self.comb_cont_layer = nn.Identity()
        if use_comb_cont:
            self.comb_cont_layer = ContFeatEmbedding(
                output_dim,
                comb_cont_dim,
                use_bias,
                dropout
            )

            self.output_dim = output_dim * comb_cont_dim

        self.act = nn.Identity()
        if activation is not None:
            self.act = get_activation_fn(activation)

        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        """
        fix directly without output temp from each cont layer
        :param inputs: batch_size, num_cont_feat
        :return:
        """
        x = self.cont_layer(inputs)
        x = self.comb_cont_layer(x)
        x = self.act(x)
        return self.projector(x)


class ConcatEmbFeatureGroup(nn.Module):
    def __init__(self,
                 output_dim,
                 activation: str = None,
                 dropout: float = 0.1,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        self.output_dim = output_dim

        self.act = nn.Identity()
        if activation is not None:
            self.act = get_activation_fn(activation)

        self.dropout = nn.Identity()
        if dropout > 0:
            self.dropout = nn.Dropout(p=dropout)

        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs: Tensor):
        """
        :param inputs: [batch_size, sum(emb_feat_output)]
        :return:
        """
        x = self.dropout(inputs)
        x = self.act(x)
        return self.projector(x)


class DeepDNNGroup(DNN):
    def __init__(self,
                 output_dim: int,
                 hidden_dims: List[int],
                 activation: str,
                 use_bn: bool,
                 use_bn_last: bool,
                 use_ln_first: bool,
                 dropout: Optional[Union[float, List[float]]] = 0.1,
                 projector: nn.Module = None,
                 **_):
        super().__init__(hidden_dims, activation, dropout, use_bn, use_bn_last, use_ln_first)
        self.projector = nn.Identity()
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim
        assert hidden_dims[-1] == output_dim

    def forward(self, inputs: Tensor) -> Tensor:
        # all feature must be cat to tensor before fitting to this function
        return self.projector(super().forward(inputs))


class DenseCatFeatureGroup(nn.Module):
    def __init__(self,
                 cat_modules: List[AdvCatFeature],
                 num_modules: int,
                 projector: nn.Module = None,
                 **_):
        super().__init__()
        assert len(cat_modules) == num_modules
        self.cat_modules = cat_modules
        self.projector = nn.Identity()
        self.output_dim = sum([cat_modules[i].output_dim for i in range(len(cat_modules))])
        if projector is not None:
            self.projector = projector
            self.output_dim = projector.output_dim

    def forward(self, inputs):
        raise NotImplementedError()


class DenseCatFeatureGroup3(DenseCatFeatureGroup):
    def __init__(self,
                 cat_modules: List[AdvCatFeature],
                 projector: nn.Module = None,
                 **_):
        super().__init__(cat_modules,
                         3,
                         projector,
                         **_)

    def forward(self, inputs):
        return torch.cat([
            self.cat_modules[0](inputs),
            self.cat_modules[1](inputs),
            self.cat_modules[2](inputs),
        ], dim=1)


class DenseCatFeatureGroup4(DenseCatFeatureGroup):
    def __init__(self,
                 cat_modules: List[AdvCatFeature],
                 projector: nn.Module = None,
                 **_):
        super().__init__(cat_modules,
                         4,
                         projector,
                         **_)

    def forward(self, inputs):
        return torch.cat([
            self.cat_modules[0](inputs),
            self.cat_modules[1](inputs),
            self.cat_modules[2](inputs),
            self.cat_modules[3](inputs),
        ], dim=1)
