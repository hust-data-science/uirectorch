import warnings
from typing import Optional

import hydra
import pyrootutils
from lightning import LightningModule
from omegaconf import DictConfig

warnings.filterwarnings("ignore")
pyrootutils.setup_root(__file__, indicator=".project-root", pythonpath=True)
# ------------------------------------------------------------------------------------ #
# the setup_root above is equivalent to:
# - adding project root dir to PYTHONPATH
#       (so you don't need to force user to install project as a package)
#       (necessary before importing any local modules e.g. `from reclib import utils`)
# - setting up PROJECT_ROOT environment variable
#       (which is used as a base for paths in "config/paths/default.yaml")
#       (this way all filepaths are the same no matter where you run the code)
# - loading environment variables from ".env" in root dir
#
# you can remove it if you:
# 1. either install project as a package or move entry files to project root dir
# 2. set `root_dir` to "." in "config/paths/default.yaml"
#
# more info: https://github.com/ashleve/pyrootutils
# ------------------------------------------------------------------------------------ #

from reclib import utils

log = utils.get_pylogger(__name__)


@hydra.main(version_base="1.3", config_path="../config", config_name="train.yaml")
def main(cfg: DictConfig) -> Optional[float]:
    # apply extra utilities
    # (e.g. ask for tags if none are provided in cfg, print cfg tree, etc.)
    utils.extras(cfg)

    # train the model
    model: LightningModule = hydra.utils.instantiate(cfg.model)


if __name__ == "__main__":
    main()
